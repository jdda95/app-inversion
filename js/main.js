document.addEventListener('DOMContentLoaded', function(){
  $("body").fadeIn("fast");
  createPieChartPortfolio();
  createGoals(goalsList, createGoalGraph);
});
// SLIDE SLICK PORTFOLIO SLIDE
$('.single-item').slick({
    infinite: false,
    dots: true,
    arrows: false,
    slidesToShow: 1,
    slidesToScroll: 1
});
$('.portfolioSlide').slick({
    infinite: true,
    dots: false,
    arrows: false,
    slidesToShow: 1,
    slidesToScroll: 1
});
$('#left').click(function(){
  $('.portfolioSlide').slick('slickPrev');
});
$('#right').click(function(){
  $('.portfolioSlide').slick('slickNext');
});

function createPieChartPortfolio() {
  var width = 120, height = 122, margin = 5;
  var radius = Math.min(width, height) / 2 - margin;
  var svg = d3.select("#my_dataviz")
              .append("svg")
              .attr("width", width)
              .attr("height", height)
              .append("g")
              .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")")
  
  var data = {a: 10, b: 10, c: 8, d:5, e:5, f:5, g:4, h:3, i: 3, j: 3};
  var opacity = [1, .9, .8, .7, .6, .5, .4, .3, .2, .1];
  
  var pie = d3.pie().value(function(d) {return d.value; });
  var data_ready = pie(d3.entries(data));
  svg
    .selectAll('whatever')
    .data(data_ready)
    .enter()
    .append('g')
    .attr('class', 'pathcontainer')
    .attr('opacity', 0.4)
    .append('path')
    .attr('d', d3.arc()
      .innerRadius(45)
      .outerRadius(radius)
    )
    .attr("fill", '#58506B')
    .attr("stroke", "white")
    .style("stroke-width", "1px")
    .attr("opacity", function (d, i) { 
        return opacity[i]; 
  });
  var chart = document.querySelector('#my_dataviz');
  var actives = chart.dataset.active;
  for (let index = 0; index < actives; index++) {
      var groupChart = [];
      groupChart.push(chart.querySelector('g').querySelectorAll('.pathcontainer')[index]);
      groupChart.forEach(path => {
          path.style.transform = 'scale(1.03)';
          path.setAttribute('opacity', 1);
          path.querySelector('path').setAttribute('fill', '#41CF69');
      })
  }
}

function createGoalGraph(container) {
  var wrapper = document.getElementById(container);
  var start = 0;
  var end = wrapper.dataset.percentage;
  var colours = { fill: '#00C0FF', track: '#FAF9FD', text: '#54D1ED', stroke: '#54D1ED' };
  var radius = 25;
  var border = 5;
  var strokeSpacing = 0;
  var endAngle = Math.PI * 2;
  var boxSize = radius * 2;
  var formatText = d3.format('.0%');
  var count = end;
  var progress = start;
  var step = end < start ? -0.01 : 0.01;
  
  // Creación del circulo
  var circle = d3.arc()
              .startAngle(0)
              .innerRadius(radius)
              .outerRadius(radius - border);
  
  //Configuración del SVG qe va a contener nuestro circulo
  var svg = d3.select(wrapper)
    .append('svg')
    .attr('width', boxSize)
    .attr('height', boxSize);
  
  // Agrupar elementos en el SVG
  var g = svg.append('g')
    .attr('transform', 'translate(' + boxSize / 2 + ',' + boxSize / 2 + ')');
  
  //Valor que se lleva hasta el momento
  var track = g.append('g').attr('class', 'radial-progress');
  track.append('path')
    .attr('class', 'radial-progress__background')
    .attr('fill', colours.track)
    .attr('stroke', colours.stroke)
    .attr('stroke-width', 0)
    .attr('d', circle.endAngle(endAngle));
  
  //Dando Colores a los elementos
  var value = track.append('path')
  .attr('class', 'radial-progress__value')
  .attr('fill', colours.fill)
  .attr('stroke', colours.stroke)
  .attr('stroke-width', strokeSpacing + 'px')
    .style("stroke-linecap", "round");
  
  //Valor del texto
  var numberText = track.append('text')
  .attr('class', 'radial-progress__text')
  .attr('fill', colours.text)
  .attr('text-anchor', 'middle')
  .attr('dy', '.3rem');


  function update(progress) {
      value.attr('d', circle.endAngle(endAngle * progress));
      numberText.text(formatText(progress));
  } 
    
  (function iterate() {
      update(progress);
      if (count > 0) {
          count--;
          progress += step;
          setTimeout(iterate, 10);
      }
  })();
}

var goalsList = [
  {
      name: 'Viaje a Islas Canarias',
      img: 'img/isla.png',
      currentMoney: 1630000,
      total: 8000000   
  },
  {
      name: 'Macbook Pro',
      img: 'img/pc.png',
      currentMoney: 0,
      total: 7000000   
  }

];

function createGoals(goals, callback){
  var ulGoals = document.querySelector('.goals_list');
  for (let i = 0; i < goals.length; i++) {
      const goal = goals[i];
      var percentage = Math.round(goal.currentMoney*100/goal.total);
      var templateGoal = `
      <li class="goals_list_item">
          <img src="${goal.img}" alt="isla">
          <h3 class="goal_name semibold">${goal.name}</h3>
          <div class="complete">
              <div id="goal-${i}" data-percentage="${percentage}"></div> 
              <div class="money">
                  <div class="money_now semibold">$${new Intl.NumberFormat().format(goal.currentMoney)}</div>
                  <div class="money_total">$${new Intl.NumberFormat().format(goal.total)}</div>
              </div>
          </div>
      </li>`;
      ulGoals.innerHTML += templateGoal;
      setTimeout(() => {
          callback(`goal-${i}`);
      }, 10);
  }
}